import Ember from 'ember';

export function linkButton(params/*, hash*/) {
  var escaped = Ember.Handlebars.Utils.escapeExpression(params);
  return new Ember.Handlebars.SafeString('<a href=' + escaped + '</a>');
}

export default Ember.Helper.helper(linkButton);
