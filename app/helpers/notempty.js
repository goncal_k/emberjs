import Ember from 'ember';

export function notempty(params/*, hash*/) {
  if(Ember.isArray(params))
  {
    if(params[0].get('length') === 1)
    {
      return false;
    }
    else
    {
      return params;
    }
  }
  else
  {
    return params;
  }
}

export default Ember.Helper.helper(notempty);
