import { notempty } from '../../../helpers/notempty';
import { module, test } from 'qunit';

module('Unit | Helper | notempty');

// Replace this with your real tests.
test('it works', function(assert) {
  var result = notempty(42);
  assert.ok(result);
});
